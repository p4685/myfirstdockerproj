package com.dockertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFirstDockerProjApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyFirstDockerProjApplication.class, args);
	}

}
