FROM openjdk:8
VOLUME /tmp
ADD target/mydocker-proj-image.jar mydocker-proj-image.jar 
EXPOSE 8080
ENTRYPOINT ["java", "-jar" ,"mydocker-proj-image.jar"]
